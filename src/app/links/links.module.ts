import {NgModule} from '@angular/core';
import {LinksComponent} from './links.component';

@NgModule({
  declarations: [
    LinksComponent
  ],
  imports: [],
  exports: []
})
export class LinksModule {}
