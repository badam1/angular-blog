import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SignUpComponent} from './sign-up/sign-up.component';
import {SignInComponent} from './sign-in/sign-in.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    SignUpComponent,
    SignInComponent
  ]
})
export class AuthModule {
}
