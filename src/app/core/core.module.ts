import {NgModule} from '@angular/core';

import {HeaderComponent} from './header/header.component';
import {HomeComponent} from './home/home.component';
import {AppRoutingModule} from '../app-routing.module';
import {SharedModule} from '../shared/shared.module';
import { FooterComponent } from './footer/footer.component';
import {PostsModule} from '../posts/posts.module';
import {PostsComponent} from '../posts/posts.component';


@NgModule({
  declarations: [
    HeaderComponent,
    HomeComponent,
    FooterComponent
  ],
  imports: [
    SharedModule,
    AppRoutingModule,
    PostsModule,
  ],
  exports: [
    AppRoutingModule,
    HeaderComponent,
    FooterComponent,
    PostsComponent
  ],
  providers: []
})
export class CoreModule {
}
