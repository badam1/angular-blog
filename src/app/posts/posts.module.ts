import {NgModule} from '@angular/core';
import {PostListComponent} from './post-list/post-list.component';
import {PostComponent} from './post/post.component';
import {PostsComponent} from './posts.component';


@NgModule({
  declarations: [
    PostListComponent,
    PostComponent,
    PostsComponent
  ],
  imports: [],
  exports: [
    PostsComponent
  ]
})
export class PostsModule {

}
